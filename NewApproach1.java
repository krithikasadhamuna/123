

import java.text.ParseException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;




class DateAndTime {
    public Date date ;
    
    public void enterDateandTime() throws ParseException {
        
        Scanner input = new Scanner(System.in);
        String dateString ;
        System.out.println("Enter date with time string:(dd/MM/yyyy-HH:mm) ");
        dateString = input.next();
        SimpleDateFormat formatter =  new SimpleDateFormat("dd/MM/yyyy-HH:mm");
        date = formatter.parse(dateString) ;          
    }
    
    public void displayDateandTime() {
        System.out.print("Date with time is -->" + date);
        
    }   
    
    void calculateEndDateAndTime(Duration duration,DateAndTime dateAndTime) throws ParseException {
        
        String dateString ;
        
        SimpleDateFormat formatter =  new SimpleDateFormat("dd/MM/yyyy-HH:mm");
        
        dateString = formatter.format(dateAndTime.date);
        Calendar calender = Calendar.getInstance();
        calender.setTime(formatter.parse(dateString));
       
        
        calender.add(Calendar.DATE, duration.days);
        calender.add(Calendar.MINUTE, duration.mins);
        calender.add(Calendar.HOUR, duration.hours);
        
        dateString= formatter.format(calender.getTime());
        date = formatter.parse(dateString);
        
        
    }
    
}

class Duration {
    int days;
    int hours;
    int mins;
    
    void enterDuration () {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the DURATION in the format (enter only int)-->:");
        System.out.println("days:  ");
        days=input.nextInt();
        do {
            System.out.println("Enter correct hour:  ");
            hours=input.nextInt();
        }while( hours >= 24 );
        do {
            System.out.println("Enter correct minutes:  ");
            mins=input.nextInt();
        }while ( mins >= 60);
    }
    
    void displayDuration() {
        System.out.println("\n DURATION -->    days:" + days + "   hours:" + hours + "   mins:" + mins);
    }
    
        
    void calculateDuration(DateAndTime date1,DateAndTime date2) {
        
        long differenceInTime;
        
        differenceInTime = date2.date.getTime() - date1.date.getTime();
        
        
        days = (int) ((differenceInTime/ (1000 * 60 * 60 * 24))% 365);
        hours = (int) ((differenceInTime/ (1000 * 60 * 60))% 24);
        mins = (int) ((differenceInTime/ (1000 * 60))% 60);             
    }
}



class Person {
    
    List<Person> friend= new ArrayList<>() ;
    public String name;
    String job;
    String location;
    List<Integer> telephoneNumber = new ArrayList<>();
    String personality;
    
    void enterPerson() {
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter the name of the person:");
        name = input.nextLine();
	    
        System.out.println("Enter the job of the person:");
        job = input.next();
	  
        System.out.println("Enter the location where you met  the person:");
        location = input.next();
	    
        System.out.println("Enter the personality of the person:");
        personality = input.next();
	    
        System.out.println("Enter the no of telephone numbers the person has(enter only int):");
        int noOfTelephoneNo = input.nextInt();
        
        for (int j=0;j<noOfTelephoneNo;j++) {
            System.out.println("Enter the tel. num ( enter only int) :");
            int newTelephoneNo = input.nextInt();
            telephoneNumber.add(newTelephoneNo);
        }
    }
    
    
    void enterFriend() {
        
        Scanner input = new Scanner(System.in);
	
	   
        System.out.println( "Does he have a friend you know of ? enter the option number ( 1.Yes / 2. No)");
	int option  = input.nextInt();
        if ( option == 1 ) {
            System.out.println( ".Direct Friend:");
            enterPerson();
            System.out.println( "friends of Friend:");
            System.out.println("No of friends, Direct friends  has(enter only int):");
            int noOfFriends = input.nextInt();
            for(int j=0 ; j<noOfFriends;j++) {
                System.out.println( (j+1) + ". Friend of Friend :"); 
                Person friendOFFriend = new Person();
                friendOFFriend.enterPerson();
                friendOFFriend.friend = null ;
                friend.add(friendOFFriend);
            }            
	}
	else {
            System.out.println( "Direct Friends:");
	    enterPerson();
	    friend = null ;
	}
    }
    
    void displayPerson(){
        
        System.out.println("Person name :  " + name);
        System.out.println("Job :  " + job );
        System.out.println("personality :  " + personality);
        System.out.println("Location :   "+ location);
        
        if(telephoneNumber.isEmpty()) {
            System.out.println("No TELEPHONE NUMBER");
        }
        else {
            telephoneNumber.forEach(telephoneNo -> {
                System.out.println("tel no:  " + telephoneNo);
            });
        }
        
        if (friend == null) {
            
            System.out.println("No Friend Of them");           
        }
        
        else {
            
            System.out.println("Friend of them:");
           
            for (Person person :friend) {
                person.displayPerson();
            }            
        }   
    }
}

class Activities {
    
    
    
    String activityName;
    int activityCost;
    DateAndTime dateActivity = new DateAndTime();
    Duration durationActivity = new Duration();
    
    void enterActivities() throws ParseException {
	    
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the activity name:");
        activityName = input.nextLine();
        
        System.out.println("Enter the activity cost(enter only int):");
        activityCost = input.nextInt();
        
        System.out.println("time details");
        
        dateActivity.enterDateandTime();
        
        durationActivity.enterDuration();
    } 
    
    
    void displayActivities (){
        
        System.out.println("Activities name:  " + activityName);
        
        System.out.println("Activities price:  " + activityCost);
        
        System.out.println("Duration details :  ");
        
        dateActivity.displayDateandTime();
        
        durationActivity.displayDuration();
    }

}


class Room {
    
    
    String roomType;
    int roomPrice;
    
    DateAndTime roomDate = new DateAndTime();
    Duration daysRoom = new Duration();
    DateAndTime roomEndDate = new DateAndTime();
    
    void enterRoom() throws ParseException {
        
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room type:");
        roomType = input.nextLine();
	    
        System.out.println("Enter the room price per day(enter only int):");
        roomPrice = input.nextInt();
	    
        System.out.println("time details");
	roomDate.enterDateandTime();
        daysRoom.enterDuration();
        roomEndDate.calculateEndDateAndTime(daysRoom,roomDate);
        
    }
    int calculateRoomtotalCost() {
        
        int totalRoomCost;
        totalRoomCost = roomPrice * daysRoom.days ;
        return totalRoomCost;
    }
    
    void displayRoom() {
        
        System.out.println("Room type:  " + roomType);
        System.out.println("Room price per day :  " + roomPrice);
        int totalRoomcost = calculateRoomtotalCost();
        System.out.println("total room cost :  " + totalRoomcost);
        System.out.println("Each Room Duration details : ");
        
        roomDate.displayDateandTime();
        daysRoom.displayDuration();
        roomEndDate.displayDateandTime();
    }
}

class Hotel {
    
    public List<Room> rooms = new ArrayList<>();
    public String hotelName;
    String hotelLocation;
    String review;
    public DateAndTime hotelStartDate = new DateAndTime();
    public DateAndTime hotelEndDate = new DateAndTime();
    public Duration hotelDuration = new Duration();
    
    void enterHotel() throws ParseException{
	    
        Scanner input = new Scanner(System.in);
	    
        System.out.println("Enter the name:  ");
        hotelName = input.nextLine();
	    
        System.out.println("Enter the location:  ");
        hotelLocation = input.nextLine();
	    
        System.out.println("Enter the review:   ");
        review = input.nextLine();
        
        System.out.println("Enter the no of rooms occupied (enter only int):  ");
        int noOfRooms = input.nextInt();
        for (int j = 1 ;j<=noOfRooms;j++){
            
            Room newRoom = new Room();
            System.out.println(j+". Room");
            newRoom.enterRoom();
            rooms.add(newRoom);
        }

        hotelStartDate = rooms.get(0).roomDate;
        hotelEndDate.calculateEndDateAndTime(hotelDuration, hotelStartDate);
        hotelDuration.calculateDuration(hotelStartDate, hotelEndDate);
        
    }
    void displayHotel() {
	        
        System.out.println("Hotel name:  "+ hotelName);
	        
        System.out.println("Hotel location:  " + hotelLocation);
	        
        System.out.println("\n Duration details: ");
        System.out.println("START DATE :  ");
        hotelStartDate.displayDateandTime();
        System.out.println("\n END DATE :  ");
        hotelEndDate.displayDateandTime();
        
        hotelDuration.displayDuration();
        
        System.out.println("\n Room:");
        int j = 0;
        for( Room eachRoom : rooms) {
            j++;
            System.out.println("Room no : " + 1);
            eachRoom.displayRoom();
        }
        
        System.out.println("\n Review :"+ review);
    }
    
    int calculateHotelCost() {
        int sum = 0;
        sum = rooms.stream().map(eachRoom -> eachRoom.calculateRoomtotalCost()).map(a -> a).reduce(sum, Integer::sum);
        return sum;
    }
}

class Travel {
    
    public String beginning;
    public String destination;
    String travelComments;
    public DateAndTime travelDate = new DateAndTime();
    public Duration durationTravel = new Duration();
    
    
    void addTravel() throws ParseException {
	    
        Scanner input = new Scanner(System.in);
	
        
        travelDate.enterDateandTime();
        durationTravel.enterDuration();
        
        System.out.println("Comment on the travel");
        travelComments = input.next();
    }
    
    void displayTravel() 
    {
        System.out.println("Starting place:  " + beginning);
        System.out.println("Destination:  " + destination);
        System.out.println("Duration details:  ");
        travelDate.displayDateandTime();
        durationTravel.displayDuration();
        System.out.println("Comments:  " + travelComments);
	
    }
}

class Place {
    
    DateAndTime placeDate = new DateAndTime();
    DateAndTime placeEndDate = new DateAndTime();
    public Duration durationPlace = new Duration();
    public String location;
    public String typeOfPlace;
    String commentOnPlace;
    public List<Activities> activities = new ArrayList<>();
    public Hotel hotel = new Hotel() ;
    
    void addPLace() throws ParseException {
            
        Scanner input = new Scanner(System.in);
	
        System.out.println("PLACE DETAILS:");
        
        System.out.println("Enter type of place");
        typeOfPlace = input.nextLine();
         
        
        System.out.println("Enter comments of PLACE");
        commentOnPlace = input.nextLine();
        
        System.out.println("Did you do any activities? enter the correct option number ( 1.Yes / 2. No)");
        int option  = input.nextInt();
        if (option == 1)
        {
            System.out.println("Enter the no of activities (enter only int):");
            int noOfActivities=input.nextInt();
            enterActivities(noOfActivities);
        }
        
        System.out.println("Did you stay in hotel? enter the correct option number ( 1.Yes / 2. No)");
        option  = input.nextInt();
        if ( option == 1 )
        {
            System.out.println("Hotel");
            hotel.enterHotel();
        }

    }
    void enterActivities(int noOfActivities) throws ParseException {
        for(int j=0;j<noOfActivities;j++) { 
            Activities newActivity = new Activities();
            newActivity.enterActivities();
            activities.add(newActivity);
        }
    }
	
    void displayPlace() {
	    
        System.out.println("Location name : "+ location );
	System.out.println("Type of place : "+ typeOfPlace);
	System.out.println("Duration details :  ");
        System.out.println("START DATE :  ");
	placeDate.displayDateandTime();
        System.out.println("\nEND DATE :  ");
        placeEndDate.displayDateandTime();
        
        durationPlace.displayDuration();
        System.out.println("Comments :  " + commentOnPlace);
        if ( activities != null ) {
            activities.forEach(eachActivity -> {
                eachActivity.displayActivities();
            });
        }
        if ( hotel != null ) {
            System.out.println("Hotel:" );
            hotel.displayHotel()  ;
        }
    }
    void displayActivitieslist() {
        
        if (activities.isEmpty()) {
            System.out.println("NO ACTIVITIES DONE");
        }
        else {
            int i=1;
            for (Activities eachActivity : activities ) {
                System.out.println( i + "." + eachActivity.activityName);
                i++;
            }
        }
        
    }
    int totalActivitiesCost() {
        if (activities.isEmpty()) {
            System.out.println("NO ACTIVITIES DONE");
            return 0;
        }
        else {
            int sum=0;
            sum = activities.stream().map(eachActivity -> eachActivity.activityCost).reduce(sum, Integer::sum);
            return sum ;
        }
    }
    
    int totalStayCost() {
        if (activities.isEmpty()) {
            System.out.println("NO ACTIVITIES DONE");
            int sum =0;
            sum =  hotel.calculateHotelCost();
            return sum;
        }
        else {
            int sum =0;
            sum = totalActivitiesCost() + hotel.calculateHotelCost();
            return sum;
        }
    }
}

class Journey {
    
    
    public String name;
    Travel travelTo = new Travel();
    Travel travelFrom = new Travel();
    public int vehicle;
    Place place = new Place();
    List<Person> peopleMet = new ArrayList<>();
    String overallComment;
    public DateAndTime startDate = new DateAndTime();
    public DateAndTime endDate = new DateAndTime();
    
    public Duration journeyDuration = new Duration();
    
    public void journeyEntry() throws ParseException {
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("Journey name :");
	name = input.nextLine();
	  
	
	System.out.println("TRIPTO :");
       
        System.out.println("Enter the starting location:");
        travelTo.beginning  = input.nextLine();
	    
        System.out.println("Enter the destination location");
        travelTo.destination = input.nextLine();
	travelTo.addTravel();
	
        do {
            System.out.println("RETURN TRIP :(enter correct date)");
            travelFrom.beginning = travelTo.destination;
            travelFrom.destination = travelTo.beginning;
            travelFrom.addTravel();
        }while(travelFrom.travelDate.date.compareTo(travelTo.travelDate.date)<0);
	   
	do {
            System.out.println("Vehicle used : (enter the correct option number) (1.car/2.bike):");
            vehicle = input.nextInt();
            
        }while(!((vehicle!=1&&vehicle==2)||(vehicle!=2&&vehicle==1))); 
	
        
        place.location = travelTo.destination;
	place.addPLace();
	    
	System.out.println("Did you meet any friends? enter the option number ( 1.Yes / 2. No)");
	int option  = input.nextInt();
	if ( option == 1 ) {
	    System.out.println("Enter the number of Direct friends :");
	    int noOfFriends = input.nextInt();
	    enterPeople(noOfFriends);
	}
        
        startDate.date= travelTo.travelDate.date;
        endDate.calculateEndDateAndTime(travelFrom.durationTravel,travelFrom.travelDate);
        journeyDuration.calculateDuration(startDate, endDate);
        
        place.placeDate.calculateEndDateAndTime(travelTo.durationTravel,travelTo.travelDate);
        place.placeEndDate = travelFrom.travelDate;
        place.durationPlace.calculateDuration(place.placeDate,place.placeEndDate);
        addJourneyComment();
    }
    
    void displayJourney() {
	   
	    
	System.out.println("Journey name" + name);
	System.out.println("Journey Duration" );
        
	System.out.println("Start date :" );
        startDate.displayDateandTime();
        System.out.println("End date :" );
        endDate.displayDateandTime();
        
        
	System.out.println("Journey Duration : \n" );
        journeyDuration.displayDuration();
	    
	    
	displayTravel();
        
        System.out.println("\nPLACE :" );
	    
	place.displayPlace();
	    
	if ( peopleMet.isEmpty() ) {
	        
	      System.out.println("\nNO FRIENDS MET" );  
	}
        else {
            System.out.println("\nPEOPLE MET :" );
            displayPeopleMet();
        }
	if(vehicle == 1) {
            System.out.println("\nVehicle: car " );
        }    
        else {
            System.out.println("\nVehicle: bike ");
        }
	
	    
	System.out.println("\nJourney overallComment :  " +overallComment);
    
    }
    
    void enterPeople(int noOfFriends) {
        
	for ( int j=0 ;j<noOfFriends;j++) {
	    Person newPerson = new Person() ;
            System.out.println((j+1)+"Direct Friend");
            newPerson.enterFriend();
            peopleMet.add(newPerson);
        }
    }
    
    
    void addJourneyComment()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the overallComment of the journey ");
	overallComment = input.nextLine();
    }
    
    void displayPeopleMet(){
        
        if(peopleMet.isEmpty()) {
            System.out.println("There are no Friends");
        }
        else {
            
            System.out.println("\nFriends :");
            int i=1;
            for(Person person : peopleMet) {
                System.out.println( i +  ". Friend :");
                person.displayPerson();
                i++;
            }
        }
    }
    
    
    void displayTravel() {
	    
	System.out.println("\n \nTRIPTO DETAILS :");
	travelTo.displayTravel();
	System.out.println("\n \nRETURN TRIP : ");
	travelFrom.displayTravel();
    
    }
    
    void directFriends() {
        
        if(peopleMet.isEmpty()) {
            System.out.println("There are no Friends");
        }
        else {
            List<Person> directFriendS = new ArrayList<>();
            for (Person person : peopleMet) {
                if(person.friend.isEmpty()) {
                    directFriendS.add(person);   
                }
            }
       
            if (directFriendS.isEmpty()) {
                System.out.println("No Direct Friends");
            }
            else {
                for (Person person : directFriendS) {
                    person.displayPerson();
                } 
            }
        }
        
    }
    void friendOfFriends() {
        
        if(peopleMet.isEmpty()) {
            System.out.println("There are no Friends");
        }
        else {
   
            List<Person> friendOfFriendS = new ArrayList<>();
            for (Person person : peopleMet) {
                if(person.friend.isEmpty()) {
                    continue;   
                }
                else {
                    for(Person friendS : person.friend) {
                        if(friendS.friend.isEmpty()) {
                            friendOfFriendS.add(friendS);
                            continue;
                        }
                        else {
                            friendOfFriendS.add(friendS);
                        }
                    }
                }
            }
            if (friendOfFriendS.isEmpty()) {
                System.out.println("No Friend OF Friends");
            }
            else {
                for (Person person : friendOfFriendS) {
                    
                    System.out.println("Friend Of Friend : ");
                    person.displayPerson();
                } 
            }        
        }
    }
}


class Travelogue {
    
    List<Journey> journeys = new ArrayList<>();
    
    void addEntry() throws ParseException {
	    
        Journey newJourney = new Journey() ;
	
        newJourney.journeyEntry();
	    
	journeys.add(newJourney);
	
    }
    
    
    void displayRecent() {
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
	
            Journey journey;
            int lastIndex = journeys.size() - 1 ;
            journey = journeys.get(lastIndex);
	    
            journey.displayJourney();
        }
     
    }
    
    
    
	
    void tripsBasedonVehicles() {
	
        Scanner input = new Scanner(System.in);
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
            List<Journey> basedOnVehicle = new ArrayList<>();
            int vehiCle;
            System.out.println("Enter the vehicle name:(enter the option number) (1.car/2.bike):");
            vehiCle= input.nextInt();
        
            for ( Journey journey : journeys) {
                int vehicle = journey.vehicle;
                if (vehiCle==vehicle) {
                    basedOnVehicle.add(journey);
                }
            }
            if (basedOnVehicle.isEmpty()) {
	      
                System.out.println("invalid vehicle or this vehicle is not used ");
            }
            else {
                basedOnVehicle.forEach(journey -> {
                    journey.displayJourney();
                });
            }
        }	
    }
    
    void tripsBasedonTerrain() {
	
        
        Scanner input = new Scanner(System.in);    
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
            List<Journey> basedOnTerrain = new ArrayList<>();
            String terrain;
            System.out.println("Enter the Terrain:");
            terrain = input.next();
        
            for ( Journey journey : journeys) {
      
                Place place = journey.place;
                if (place.typeOfPlace.equalsIgnoreCase(terrain)) {
                    basedOnTerrain.add(journey);
                }
            }
            if (basedOnTerrain.isEmpty()) {
                System.out.println("ivalid Terrain or this terrain does not exist");
            }
            else {
                for (Journey journey :basedOnTerrain) {
                    journey.displayJourney();
                }
            }
        }
    }
    
    int totalCostOfTrip() {
        Scanner input = new Scanner(System.in);
        int cost ;
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
            return 0;
        }
        else {
            System.out.println("List of Journeys: (please enter the number of needed journey)");
            int i;
            i= 1;
            for (Journey journey : journeys) {
                System.out.println( i + ". JOURNEY name:" +journey.name + "Start date: " );
                journey.startDate.displayDateandTime();
                System.out.print("  END date: ");
                journey.endDate.displayDateandTime();
            }
            
            int index = input.nextInt();
            index--;
            Journey journey;
            journey = journeys.get(index);
            
            cost = journey.place.totalStayCost();
            
        }
	return cost;    
    }
    
    int costTripStayinHotel () {
        
        Scanner input = new Scanner(System.in);
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
            return 0;
        }
        else {
            System.out.println("List of Journeys: (please enter the number of needed journey)");
            int i;
            i= 1;
            for (Journey journey : journeys) {
                System.out.println( i + ". JOURNEY name:" +journey.name + "Start date: " );
                journey.startDate.displayDateandTime();
                System.out.print("  END date: ");
                journey.endDate.displayDateandTime();
            }
            
            int index = input.nextInt();
            index--;
            Journey journey;
            journey = journeys.get(index);
            int cost ;
            Place pLace;
            pLace = new Place();
            pLace = journey.place;
            cost = pLace.hotel.calculateHotelCost();
            return cost;
        }
    }
    
    void diRectFriends() {
        Scanner input = new Scanner(System.in);
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
            System.out.println("List of Journeys: (please enter the number of needed journey)");
            int i;
            i= 1;
            for (Journey journey : journeys) {
                System.out.println( i + ". JOURNEY name:" +journey.name + "Start date: " );
                journey.startDate.displayDateandTime();
                System.out.print("  END date: ");
                journey.endDate.displayDateandTime();
            }
            
            int index = input.nextInt();
            index--;
            Journey journey;
            journey = journeys.get(index);
            journey.directFriends();
        }
    }
    
    
    void friendofFriends() {
        Scanner input = new Scanner(System.in);
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
            System.out.println("List of Journeys: (please enter the number of needed journey)");
            int i;
            i= 1;
            for (Journey journey : journeys) {
                System.out.println( i + ". JOURNEY name:" +journey.name + "Start date: " );
                journey.startDate.displayDateandTime();
                System.out.print("  END date: ");
                journey.endDate.displayDateandTime();
                i++;
            }
            
            int index = input.nextInt();
            index--;
            Journey journey;
            journey = journeys.get(index);
            journey.friendOfFriends();
        }
    }
    
    void tripsByDateRange() throws ParseException {
        
        
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
            List<Journey> listByDateRange = new ArrayList<>();
	    
            DateAndTime startDate= new DateAndTime();
            DateAndTime endDate = new DateAndTime();
	    
            System.out.println("enter start date :");
            startDate.enterDateandTime();
	
            System.out.println("enter end date :");
            endDate.enterDateandTime();
        
            journeys.forEach(journey -> {
                DateAndTime journeyStartDate = journey.startDate;
                DateAndTime journeyEndDate = journey.endDate;
                int i = startDate.date.compareTo(journeyStartDate.date);
                int j = endDate.date.compareTo(journeyEndDate.date);
                if ((i<0)&&(j>0)) {
                    listByDateRange.add(journey);
                }
            });
            if ( listByDateRange.isEmpty() ) {
                System.out.println("no trips available");
            }
            else {
                listByDateRange.forEach(journey -> {
                    journey.displayJourney();
                });
            }
        }
    }
    
    void activitiesCostandList() {
        
        Scanner input = new Scanner(System.in);
        if(journeys.isEmpty()) {
            System.out.println("There are no trips in travelogue");
        }
        else {
            System.out.println("List of Journeys: (please enter the number of needed journey)");
            int i;
            i= 1;
            for (Journey journey : journeys) {
                System.out.println( i + ". JOURNEY name:" +journey.name + "Start date: " );
                journey.startDate.displayDateandTime();
                System.out.print("  END date: ");
                journey.endDate.displayDateandTime();
            }
            
            int index = input.nextInt();
            index--;
            Journey journey;
            journey = journeys.get(index);
            int cost ;
            Place pLace= new Place();
            pLace = journey.place;
            cost = pLace.totalActivitiesCost();
            System.out.println("Activities Cost:" + cost);
            pLace.displayActivitieslist();
        }
    }
}

public class NewApproach {
    /**
     * @param args the command line arguments
     * @throws java.text.ParseException
     */
    public static void main(String[] args) throws ParseException {
        // TODO code application logic here
        Travelogue travelogue = new Travelogue();
        System.out.println("TRAVELOGUE:");
        Scanner input = new Scanner(System.in);
        int flag=0;
        
        do {
            
            
            System.out.println("MENU");
            System.out.println("Enter the number:");
            System.out.println("1.ADD entry");
            System.out.println("2.Display recent");
            System.out.println("3.Total cost of trip");
            System.out.println("4.Cost of trip Stay");
            System.out.println("5.List of trips based on Transport");
            System.out.println("6.List based on Terrain");
            System.out.println("7.Direct friends");
            System.out.println("8.Friend of friends");
            System.out.println("9.List of trips by giving range of date");
            System.out.println("10.List activities and total activities cost");
            System.out.println("11.Exit");
            int option ;
            option = input.nextInt();
            
            switch(option) {
                case 1:
                    travelogue.addEntry();
                    break;
                case 2:
                    travelogue.displayRecent();
                    break;
                case 3:
                    System.out.println("Cost :" + travelogue.totalCostOfTrip());
                    System.out.println("\n" );
                    break;
                case 4:
                    System.out.println("Cost :" + travelogue.costTripStayinHotel());
                    System.out.println("\n" );
                    break;
                case 5:
                    travelogue.tripsBasedonVehicles();
                    break;
                case 6:
                    travelogue.tripsBasedonTerrain();
                    break;
                case 7:
                    travelogue.diRectFriends();
                    break;
                case 8:
                    travelogue.friendofFriends();
                    break;
                case 9:
                    travelogue.tripsByDateRange();
                    break;
                case 10:
                    travelogue.activitiesCostandList();
                    break;
                default :
                    flag = 1;       
            }    
        } while(flag ==0);
        
        
    }
}

    